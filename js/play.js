$(document).on("ready",function(){
	var imagen = $("#img");
	var width = imagen.css("width");
	imagen.css("height",width);

	$("#play").click(function(){
		$( this ).toggleClass( "hidden" );
		$( "#pausa" ).toggleClass( "hidden" );
	});

	$("#pausa").click(function(){
		$( this ).toggleClass( "hidden" );
		$( "#play" ).toggleClass( "hidden" );
	});


	$("#stop").click(function(){
		$("#play").removeClass("hidden");
		$("#pausa").addClass("hidden");
	});

	setInterval(function(){
		var desc_play_height= $("div.descripcion-play").css("height");
		var dos = $("#img").height();
		var width = $("#img").width();

		$(".descripcion-play").css("margin-top","-"+desc_play_height);
		$(".descripcion-play").css("width",width+"px");
		//$(".volumen").css("margin-top","-"+dos*.9+"px");
		$(".botones").css("margin-top","-"+dos*.7+"px");
	},500);

});