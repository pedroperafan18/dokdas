
		$(document).ready(function(){
			var base = "http://localhost/Dokdasco/";
			function fregistro_usuario() {
					var registro_usuario = $("#registro_usuario").val();
					if(registro_usuario!=null){
						$.ajax({
							url:base+"inicio/verificar_usuario",
							type:"get",
							data:{usuario:registro_usuario}
						}).done(function(msg){
							$("div.usuario-malo").html(msg);
						});
					}
			}
			function fregistro_email() {
					var registro_email = $("#registro_email").val();
					if(registro_email!=null){
						$.ajax({
							url:base+"inicio/verificar_correo",
							type:"get",
							data:{correo:registro_email}
						}).done(function(msg){
							$("div.email-malo").html(msg);
						});
					}
			}
			function fregistro_password () {

					var registro_password = $("#registro_password").val();
					if(registro_password!=null){
						$.ajax({
							url:base+"inicio/verificar_password",
							type:"get",
							data:{password:registro_password}
						}).done(function(msg){
							$("div.password-malo").html(msg);
						});
					}
			}
			function fregistro_reemail(){
					var uno = $("#registro_email").val();
					var dos = $("#registro_reemail").val();
					if(uno!=dos){
						$("div.email-malo-dos").html("<b class='text-danger'>Los Correos Electronicos no son iguales</b>");
					}else{
						$("div.email-malo-dos").html("");
					}
			}
			$("#registro_usuario").change(function(){
				fregistro_usuario();
			});

			$("#registro_email").change(function(){
				fregistro_email();
			});

			$("#registro_password").change(function(){
				fregistro_password();
			});

			$("#registro_reemail").change(function(){
				fregistro_reemail();
			});
			
			setInterval(function(){
				var registro_usuario_dos = $("#registro_usuario").val();
				var registro_email_dos = $("#registro_email").val();
				var registro_password_dos = $("#registro_password").val();
				var dos = $("#registro_reemail").val();
				if(registro_usuario_dos!=""){
					fregistro_usuario();
				}
				if(registro_email_dos!=""){
					fregistro_email();
				}
				if(registro_password_dos!=""){
					fregistro_password();
				}
				if(dos!=""){
					fregistro_reemail();
				}
				if(registro_usuario_dos!=""&&registro_email_dos!=""&&registro_password_dos!=""){
					$("#Registro_boton").prop("disabled", false);
				}
			},1000);
		});