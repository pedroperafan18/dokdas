<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <span class="link navbar-brand dokdas" data-url="<?php echo base_url();?>">Dokdas</span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 

      <form class="navbar-form navbar-left" role="search" action="<?php echo base_url();?>buscar" method="post">
        <div class="form-group">
        	<div class="input-group">
			  <input name="busqueda" type="search" class="form-control" style="width:350px">
			  <button type="submit" class="input-group-addon" >
			  	<span class="glyphicon glyphicon-search" aria-hidden="true" style="font-size:18px;"></span>
			  </button>
			  
        	</div>
        </div>
      </form>

      <ul class="nav navbar-nav navbar-right">
        <?php 
        if(isset($sesion)){
        
		    if($sesion==FALSE){
		    ?>
		    <li><a data-url="<?php echo base_url();?>registro">Registrarse</a></li>
		    <li><a href="#" data-toggle="modal" data-target="#IniciarSesion">Iniciar Sesión</a></li>
		    <?php }else{?>
		    <li><a href="<?php echo base_url();?>"><i class="fa fa-home"></i> Inicio</a></li>
		    <li class="dropdown">
		      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		        <?php echo $nombre;?> <span class="caret"></span>
		      </a>
		      <ul class="dropdown-menu" role="menu">
		        <li><a href="<?php echo base_url();?>perfil"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Ver Perfil</a></li>
		        <li><a href="#"><i class="fa fa-cog"></i> Configuración</a></li>
		        <li class="divider"></li>
		        <li><a href="<?php echo base_url();?>index.php/inicio/salir"><i class="fa fa-power-off"></i> Cerrar Sesión</a></li>
		      </ul>
		    </li>
		    <?php 
			}
		 }?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="row-fluid">
	<div class="col-md-12" style="height:60px;"></div>
</div>
