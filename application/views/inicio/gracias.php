<div class="row-fluid">
	<div class="col-md-12">
		<h1>¡Gracias por registrarse en Dokdas!</h1>
		<div style="font-size:20px">Primeros pasos</div>
		<a href="<?php echo base_url();?>">Saltar</a>
		
		<div class="row-fluid">
			<div class="col-md-4 text-center well">
				<h3>1.-Carga una foto de perfil</h3>
				
				<img src="<?php echo $imagen;?>" alt="" class="img-responsive" />
				<form action="<?php echo base_url();?>configuracion/subir_foto/gracias" method="post" enctype="multipart/form-data">
					<div class="form-group">
					    <input type="file" style="margin-top:10px;opacity: 0;cursor:pointer" name="foto">
					    <div class="btn btn-primary  btn-block" style="margin-top: -25px;">
					    	Elegir Foto	
					    </div>
					</div>
					<button type="submit" class="btn btn-success">Cambiar Foto</button>
				</form>
			</div>
			<div class="col-md-4 text-center well">
				<h3>2.-Completar tu perfil</h3>
				<form id="config" >
					<div class="form-group">
					    <label for="" class="col-md-12 col-sm-12 control-label">Fecha de nacimiento</label>
					    <div class="col-md-12 col-sm-12">
					    	<input type="date" class="form-control"  min="2013-01-01" max="2013-12-31"/>
					    </div>
					</div>
					
					<div class="form-group">
					    <label for="" class="col-md-12 col-sm-12 control-label">Género Musical</label>
					    <div class="col-md-12 col-sm-12">
					    	<select class="form-control" id="genero">
					    		<option value="1">Otro</option>
					    		<option value="2">Alternativo</option>
					    		<option value="3">Classica</option>
					    		<option value="4">Country</option>
					    		<option value="5">DJ</option>
					    		<option value="6">Dubstep</option>
					    		<option value="7">Electronica</option>
					    		<option value="8">Folk</option>
					    		<option value="9">Funk</option>
					    		<option value="10">Hip Hop</option>
					    		<option value="11">Indie</option>
					    		<option value="12">Instrumental</option>
					    		<option value="13">Metal</option>
					    		<option value="14">Pop</option>
					    		<option value="15">Punk</option>
					    		<option value="16">Rap</option>
					    		<option value="17">Rock</option>
					    		<option value="18">Ska</option>
					    	</select>
					    </div>
					</div>
					<div class="form-group">
					    <div class="col-md-12 col-sm-12">
					    	<select class="form-control" id="instrumento">
					    		<option value="1">Voz</option>
					    		<option value="2">Guitarra</option>
					    		<option value="3">Guitarra Acustica</option>
					    		<option value="4">Guitarra Electrica</option>
					    		<option value="5">Bajo</option>
					    		<option value="6">Bateria</option>
					    		<option value="7">Teclado</option>
					    		<option value="8">Sintetizador</option>
					    		<option value="9">Piano</option>
					    		<option value="10">DJ</option>
					    		<option value="11">Violin</option>
					    		<option value="12">Cello</option>
					    		<option value="13">Saxofon</option>
					    		<option value="14">Compositor</option>
					    		<option value="15">Flauta</option>
					    		<option value="16">Arreglista</option>
					    		<option value="17">Ingeniero en Audio</option>
					    	</select>
					    </div>
					</div>
					<button id="enviar" type="button" class="btn btn-success btn-block">Guardar Información</button>
				</form>
			</div>
		</div>
	</div>
</div>