<div class="row-fluid">
	<div class="col-md-10 well col-md-offset-1 text-center" style="padding-top:30%;color:#fff;background-image:url(<?php echo base_url();?>img/error.jpg);background-size:100% 100%;">
		<h1>
			<i class="fa fa-times-circle fa-lg" style="color:red;"></i>  
			Esta página no está disponible </h1>
		<h3>
			<i class="fa fa-frown-o fa-5x"></i>
		</h3>
	</div>
</div>