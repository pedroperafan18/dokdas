<?php echo $header;?>
<body>
	<?php echo $menu;?>
	
	<div class="container">
		<div class="row-fluid">
			
			<div class="col-md-8 col-md-offset-2 well">
				<h3 class="text-center">
				<?php
				if(isset($error)){
					switch ($error) {
						case '1':
							echo "Ya estas Logeeado";
							break;
						case 2:
							echo 'Ingresa correctamente todos los campos';
							break;
						default:
							
							break;
					}
				}
				?>
				</h3>
			<?php
			if(isset($error_correo)){
				echo $error_correo;
			}
			if(isset($error_usuario)){
				echo $error_usuario;
			}
			if(isset($error_password)){
				echo $error_password;
			}
			if(isset($error_vacio)){
				echo $error_vacio;	
			}
			if(isset($form)){
				echo $form;
				
			}
			?>
				
			</div>
		</div>
	</div>
	
	<?php echo $javascript;?>
</body>
</html>