<?php echo $header;?>
<body>
	<?php echo $modal_inicio;?>
	<?php echo $modal_registro;?>
	<?php echo $menu;?>
	<div class="container">
		<div class="row-fluid">
			<div class="col-md-6 col-md-offset-3 text-center">
				<h3 class="text-danger">
					<?php
					if(isset($numero)){
						switch ($numero) {
							case 1:
								echo "Llena correctamente todos los campos";
								break;
							case 2:
								echo "Correo no relacionado con ningun usuario";
								break;
							case 3:
								echo "Usuario y/o contraseña incorrectos"; 
								break;
							default:
								echo "Error desconocido";
								break;
						}
					}
					?>
				</h3>
			</div>
		
			<div class="col-md-8 col-md-offset-2  well well-sm">
				<?php echo $form;?>
			</div>
		</div>
	</div>
	<?php echo $javascript;?>
</body>
</hmtl>