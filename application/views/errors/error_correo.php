<?php echo $header;?>
<body>
	<?php echo $menu;?>
	<div class="container">
		<div class="col-md-8 col-md-offset-2 text-center">
			<h1>UPPS! Hubo un error , por favor vuelve a intentar. <br><i class="fa fa-meh-o fa-5x"></i> </h1><br>
			<a href="<?php echo base_url();?>index.php/recuperar/index">Volver a intentar</a>
		</div>
	</div>
</body>
</html>