<nav id="player" class="navbar navbar-inverse navbar-fixed-bottom" style="border:none;padding-top: 5px;">
  <div class="container">
  	<div class="row-fluid">
  		
  		<div class="col-md-8">
  			<div class="row-fluid">
  				<div class="col-md-4 col-sm-4 col-xs-12 hidden-xs">
  					<div class="row">
  						<div class="col-md-4 col-xs-4 col-sm-4">
  							<img id="album" src="<?php echo base_url();?>img/perfil.jpg" alt="" class="img-responsive" />
  						</div>
  						<div class="col-md-8 col-xs-8 col-sm-8">
  							<div style="margin-top:5px; ">
  								<button class="btn btn-default btn-circle"><i class="fa fa-step-backward"></i></button>
  								<!--
                  <button id="play" class="btn btn-default btn-circle btn-lg"><i class="fa fa-lg fa-play"></i></button>
                  
  								<button id="pausa" class="btn btn-default btn-circle btn-lg"><i class="fa fa-lg fa-pause"></i></button>
  								-->
                  <span id="play" class="fa-stack fa-2x" ><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-play fa-stack-1x"></i></span>
                  <span id="pausa" class="fa-stack fa-2x" style="display:none"><i class="fa fa-circle fa-stack-2x fa-inverse"></i><i class="fa fa-pause fa-stack-1x"></i></span>
                  <button class="btn btn-default btn-circle"><i class="fa fa-step-forward"></i></button>
  							</div>
  						</div>
  					</div>
  					
  				</div>
  				<div class="col-md-8 col-xs-12 col-sm-8 ">
  					<div style="margin-top:10px;color:#fff">
	  					<input class="hidden-xs" type='range' id='progreso' min='0' max='100' step='.1' value='0' style="width:100%;border:none">
	  					<div class="visible-xs-inline">
                  <div class="play_pause">
                    
                  </div>
              </div>
              <span id="titulo"></span> 
	  					<span id="usuario" style="font-style: italic"></span>
              
  					</div>
  				</div>
  			</div>
  		</div>
  		<div class="col-md-2 col-xs-4 hidden-sm hidden-xs">
  			<div style="margin-top:10px; ">
  				<input type='range' id='volumen' min='0' max='1' step='0.1' value='1' style="width:100%;border:none">
  			</div>
  		</div>
  		<div class="col-md-2 hidden-sm hidden-xs">
  			<div class="btn-group" role="group" aria-label="...">
        <?php if(isset($sesion)&&$sesion==TRUE){?>
        <button type="button" class="btn btn-default"><i class="fa fa-heart"></i></button>
        <button type="button" class="btn btn-default"><i class="fa fa-star"></i></button>
        <button type="button" class="btn btn-default"><i class="fa fa-share"></i></button>
        <button id="loop" type="button" class="btn btn-default"><b>1</b></button>
        <?php }else{ ?>
        <button type="button" class="btn btn-default"><i class="fa fa-share"></i></button>
        <button id="loop" type="button" class="btn btn-default"><b>1</b></button>
        <?php } ?>
  			<button id="minimizar" type="button" class="btn btn-default"><i class="fa fa-minus-square-o"></i></button>
			</div>
  		</div>
  	</div>
    <audio id="music" src=""></audio>
  </div>
</nav>