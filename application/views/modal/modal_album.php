  <div class="modal fade" id="Album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear Album</h4>
      </div>
      <div class="modal-body">

		  <div class="form-group">
		    <label for="nombre_album" class="col-sm-3 control-label">Nombre del Álbum</label>
		    <div class="col-sm-9">
		      <input type="text" class="form-control" id="nombre_album" placeholder="Nombre del Álbum">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9">
		      <button type="submit" id="crear_album" class="btn btn-success">Crear Álbum</button>
		    </div>
		  </div>

      </div>
    </div>
  </div>
</div>