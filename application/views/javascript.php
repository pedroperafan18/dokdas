<?php
foreach ($script as $key => $value) {
echo '<script src="'.base_url().'js/'.$value.'.js"></script>';
}
?>
<script type="text/javascript" charset="utf-8">

$(function() {
	$(".play_pause").click(function () {
		$(".play_pause").cambiariconos();
	});

	var musica = document.getElementById('music');
	$.fn.cambiariconos = function(){
		var html = $(this).html().trim();
		if(html == '<span class="fa-stack fa-lg"><i class="fa fa-play-circle fa-stack-2x"></i></span>'){
			html = '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pause fa-stack-1x" style="color:#333333"></i></span>';
		}else{
			html = '<span class="fa-stack fa-lg"><i class="fa fa-play-circle fa-stack-2x"></i></span>';
		}
		$("[data-music]").html('<span class="fa-stack fa-lg"><i class="fa fa-play-circle fa-stack-2x"></i></span>');
		$(this).html(html);
	}

	function reproducir(){
		musica.play();
		$("#play").css("display","none");
		$("#pausa").css("display"," ");	
		setInterval(progreso,1000);
	}
	function pausa() {
		musica.pause();
		$("#play").css("display"," ");
		$("#pausa").css("display","none");	
	}

	function progreso(){
		
		var valor = (musica.currentTime * 100) / musica.duration;
		$('#progreso').val(valor);
	}



	window.onpopstate = function(event) {
	  //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
	  	var href = document.location;
	    history.pushState({}, '', href);
	   	$.ajax({
			type : "get",
			url : href,
		}).done(function(result){
			$("#contenedor").html(result);
		});
	};
	
	
	$("body").delegate("[data-url]","click",function(){
		var url = $(this).data( "url");
		
		$.ajax({
			url : url,
			type : "get"
		}).done(function(result){
			$("#contenedor").html(result);
			history.pushState({}, '', url);
		});
	});
	
	$("body").delegate("[data-music]","click tap",function(){
		var id = $(this).data("music");
		$(this).attr("id","reproduciendo");
		$.ajax({
			url : "<?php echo base_url();?>reproductor/getCancion",
			type : "get",
			data : {ID:id}
		}).done(function(result){
			var cancion = result.substr(0,result.search("~"));
			result = result.substr(result.search("~")+1);
			
			var nombre = result.substr(0,result.search("~"));
			result = result.substr(result.search("~")+1);
			var album = result.substr(0,result.search("~"));
			result = result.substr(result.search("~")+1);
			var foto = result.substr(0,result.search("~"));
			result = result.substr(result.search("~")+1);
			var usuario = result.substr(0,result.search("~"));
			result = result.substr(result.search("~")+1);
			var id = result.substr(0,result.search("~"));
			
			cancion  = "<?php echo base_url();?>music/"+id+"/"+cancion;
			$("#music").attr("src",cancion);
			
			if(foto.length == 0){
				foto = "<?php echo base_url();?>img/perfil.jpg";
			}else{
				foto = "<?php echo base_url();?>music/"+id+"/"+foto;
			}

			$("#album").attr("src",foto);
			$("#titulo").text(nombre);
			$("#usuario").text(usuario);
			reproducir();
			  $('.top-right').notify({
			    message: { text: nombre+" - "+usuario },
			    type : 'bangTidy',
			   	closable: false
			  }).show();
		});
		$(this).cambiariconos();
		
	});
	
	$("#pausa").click(function(){
		$("[data-music]").html('<span class="fa-stack fa-lg"><i class="fa fa-play-circle fa-stack-2x"></i></span>');
		pausa();
	});
	
	$("#play").click(function(){
		var src = $("#music").attr("src");
		//if(src){

		reproducir();
		//}
	});
	
	$('#progreso').change(function(){
		musica.currentTime = ($(this).val() * musica.duration)/100;
	});

	$("#loop").click(function(){
		//musica.loop = (musica.loop) ? false:true;
		if(musica.loop==true){
			musica.loop = false;
		}else{
			musica.loop = true;
		}
		$(this).toggleClass("btn-success");
	});

	$( "#minimizar" ).click(function() {
	  $( "#player" ).slideToggle( "fast" );
	});

});
	</script>
