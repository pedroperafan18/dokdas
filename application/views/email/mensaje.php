<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bienvenido</title>
    <link rel="stylesheet" href="http://getbootstrap.com/dist/css/bootstrap.min.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
	<link rel="stylesheet" href="http://dokdas.com/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  	<div class="container-fluid">
    	<div class="navbar-header">
      	<img src="http://dokdas.com/img/logo.png" style="width: 50px;" />
    	</div>
	</div>
</nav>
<div class="row-fluid">
	<div class="col-md-12" style="height:60px;"></div>
</div>
<div class="container">
    <div class="row-fluid">
        <div class="span8 offset2 well">
           <h2>Hola <?=$Nombre?> </h2>
           <p style="margin:10px;">
           Te damos la bienvenida a la comunidad de dokdas. <br>
           Aqui podras compartir fotos con tus amigos, te podran hacer preguntas y conocer m&aacute;s sobre ti.
           </p>
           <a class="btn btn-primary btn-large btn-block" href="http://www.dokdas.com/v/clave/<?php echo $Clave;?>">Confirmar tu correo electrónico</a><br/>
           Gracias por registrarte. <br/>
           El equipo de Dokdas<br/>
        </div>
        <div class="span8 offset2">
        Si recibiste este mensaje por error y no te registraste para una cuenta de Dokdas, haz click <a href="http://www.dokdas.com/ayuda/cuenta/1" target="_blank">esta cuenta no es m&iacute;a.</a>
        </div>
    </div>
</div>
</body>
</html>