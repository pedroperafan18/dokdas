<form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>cancion/album">
  <div class="form-group">
    <label for="album" class="col-sm-3 control-label">Nombre del álbum</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="album" placeholder="Nombre del Album" name="nombreAlbum">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <button id="Inicio_boton" type="submit" class="btn btn-primary btn-block btn-large" >Crear Álbum</button>
    </div>
  </div>
</form>