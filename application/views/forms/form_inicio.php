<form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>inicio/iniciar<?php echo $ruta = (isset($tienda)) ? "/tienda" : "/inicio"?>">
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Correo Electrónico</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="email" placeholder="Correo Electrónico" name="inicio_usuario" value="<?php if(isset($correo)){ echo $correo;}?>">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-3 control-label">Contraseña</label>
    <div class="col-sm-9">
      <input type="password" class="form-control" id="password" placeholder="Contraseña" name="inicio_password" >
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="inicio_recordar" id="recordar"> Acuerdate de mí
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <a href="<?php echo base_url();?>recuperar<?php if(isset($correo)){ echo '?correo='.$correo;}?>">Olvidé mi contraseña</a>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <button id="Inicio_boton" type="submit" class="btn btn-primary btn-block btn-large" >Iniciar Sesión</button>
    </div>
  </div>
</form>