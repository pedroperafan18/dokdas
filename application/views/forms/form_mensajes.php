<form class="form-horizontal" action="<?php echo base_url();?>perfil/mensaje/<?php echo $id;?>" method="post">
	<div class="form-group">
		<label class="col-sm-2 control-label">Mensaje</label>
		<div class="col-sm-10">
			<textarea name="mensaje" class="form-control"></textarea>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-success" >Enviar</button>
		</div>
	</div>
</form>