
<form class="form-horizontal" action="<?php echo base_url()."inicio/registrando";?>" role="form" method="post">
  <div class="form-group">
    <label for="usuario" class="col-sm-3 control-label">Usuario</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="registro_usuario" name="registro_usuario" placeholder="Usuario" value="<?php if(isset($usuario)){echo $usuario;}?>">
      <div class="usuario-malo">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="nombre" class="col-sm-3 control-label">Nombre</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="registro_nombre" name="registro_nombre" placeholder="Nombre" value="<?php if(isset($nombre)){echo $nombre;}?>">
    </div>
  </div>
  <div class="form-group">
    <label for="apellidos" class="col-sm-3 control-label">Apellidos</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="registro_apellidos" name="registro_apellidos" placeholder="Apellidos" value="<?php if(isset($apellidos)){echo $apellidos;}?>">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Correo Electrónico</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="registro_email" name="registro_email" placeholder="Correo Electrónico" value="<?php if(isset($correo)){echo $correo;}?>">
      <div class="email-malo">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="reemail" class="col-sm-3 control-label">Repetir Correo Electrónico</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="registro_reemail" name="registro_reemail" placeholder="Repetir Correo Electrónico" value="<?php if(isset($recorreo)){echo $recorreo;}?>">
      <div class="email-malo-dos">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-3 control-label">Contraseña</label>
    <div class="col-sm-9">
      <input type="password" class="form-control" id="registro_password" name="registro_password" placeholder="Contraseña" value="<?php if(isset($password)){echo $password;}?>">
      <div class="password-malo">
        
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <button id="Registro_boton" type="submit" class="btn btn-primary btn-block btn-large" name="submit" disabled="disabled">Registrarse</button>
    </div>
  </div>
</form>