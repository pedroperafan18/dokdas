<form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>recuperar/enviar">
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Correo Electrónico</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="email" placeholder="Correo Electrónico" required="required" name="recuperar_usuario" value="<?php if(isset($correo)){ echo $correo;}?>">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <button id="Inicio_boton" type="submit" class="btn btn-primary btn-block btn-large" >Recuperar Contraseña</button>
    </div>
  </div>
</form>