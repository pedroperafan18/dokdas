<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reproductor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mreproductor');
	}

	public function getcancion()
	{
		if($this->input->is_ajax_request()){
			if($this->input->get('ID')){
				$id = $this->input->get('ID');

				$query = $this->mreproductor->getCancion($id);
				foreach ($query as $key => $value) {
					foreach ($value as $keys => $values) {
						echo $values."~";
					}
				}
			}
		}
	}
	public function gettop()
	{
		if($this->input->is_ajax_request()){
			if($this->input->get('ID')){
				$id = $this->input->get('ID');
				$query = $this->mreproductor->gettop();
				foreach ($query as $key => $value) {
					foreach ($value as $keys => $values) {
						echo $values."~";
					}
					echo "*";
				}
			}
		}
	}
	
	
	public function nuevo_playlist()
	{
		# code...
	}
	public function editar_playlist()
	{
		# code...
	}
	public function eliminar_playlist()
	{
		# code...
	}

}

/* End of file reproductor.php */
/* Location: ./application/controllers/reproductor.php */