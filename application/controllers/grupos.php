<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$session["ID"] = 4;
		$dos = $this->uri->segment(2);
		$tres = $this->uri->segment(3);

		if(($dos!=NULL&&is_numeric($dos))||($tres!=NULL&&is_numeric($tres))){
			$numero =  (is_numeric($dos)) ? $dos : $tres;
			echo "Ver grupo de $numero";
			if($numero==$session["ID"]){
				redirect(base_url()."grupo");
			}
		}else{
			echo "ver mi grupo";
		}
	}

	public function seguir(){
		# code...
	}
	public function setranking(){
		# code...
	}
	public function getranking(){
		
	}	

}

/* End of file grupos.php */
/* Location: ./application/controllers/grupos.php */