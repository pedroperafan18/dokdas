<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('dokdas');
		$this->load->library(array('form_validation','session'));
		$this->load->model('minicio');
		$this->lang->load('inicio', 'spanish');
	}
	
	private function recordar(){
		if(!$this->session->userdata('Dokdas_User_ID')){
			$coockie_id = $this->input->cookie('Dokdas_User_ID'); 
			$coockie_correo = $this->input->cookie('Dokdas_User_Correo');
			if($this->minicio->get_true_by_id_correo($coockie_id,$coockie_correo)){
				$ip = $this->input->ip_address();
				$datasession = array(
					'Dokdas_User_ID' => $coockie_id,
		        	'Dokdas_User_Correo'  => $coockie_correo,
		        	'Dokdas_User_Ip' => $ip,
		         	'Dokdas_login_ok' => TRUE
		        );
		        $this->session->set_userdata($datasession);
			}
		}
	}

	public function index()
	{

		$data_header["titulo"] = $this->lang->line('titulo');
		$data_menu["sesion"] = FALSE;
		$data_menu["registro"] = $this->lang->line('registro');

		$data_header["css"] = array("font-awesome.min","bootstrap.min","style","bootstrap-notify");
		$data_javascript["script"] = array("jquery","bootstrap.min","bootstrap-notify");

		$data_inicio["mes"] = $this->dokdas->mes(date("m"));
		$data_main["contenido"] = $this->load->view('inicio/inicio', $data_inicio, TRUE);
		
		if($this->input->is_ajax_request()){
			$this->load->view('main',$data_main);
		}else{
			$data["main"] = $this->load->view('main',$data_main,TRUE);
			$data["header"] = $this->load->view("header", $data_header, TRUE);	
			$data["menu"] = $this->load->view('menu', $data_menu, TRUE);
			$data["javascript"] = $this->load->view('javascript', $data_javascript , TRUE);
			$data["footer"] = $this->load->view('footer', '' , TRUE);
			$data["player"] = $this->load->view('player','',TRUE);
			$this->load->view('html',$data);	
		}
	}
	public function registro()
	{
		$data_header["titulo"] = "Registro";
		$data_menu["sesion"] = FALSE;
		$data_header["css"] = array("font-awesome.min","bootstrap.min","style","bootstrap-notify");
		$data_javascript["script"] = array("jquery","bootstrap.min","registro","bootstrap-notify");

		$data_contenedor["contenido"] = $this->load->view('forms/form_registro', "", TRUE);
		
		$data_extras["extras"]["nombre"] = "";
		$data_main["extras"] = $this->load->view('extras',$data_extras,TRUE);
		$data_main["contenido"] = $this->load->view('contenedores/6', $data_contenedor, TRUE);
		
		if($this->input->is_ajax_request()){
			$this->load->view('main',$data_main);
		}else{
			$data["main"] = $this->load->view('main',$data_main,TRUE);
			$data["header"] = $this->load->view("header", $data_header, TRUE);	
			$data["menu"] = $this->load->view('menu', $data_menu, TRUE);
			$data["javascript"] = $this->load->view('javascript', $data_javascript , TRUE);
			$data["footer"] = $this->load->view('footer', '' , TRUE);
			$data["player"] = $this->load->view('player','',TRUE);
			$this->load->view('html',$data);	
		}
	}
	public function inicio()
	{
		# code...
	}
	public function registrando()
	{
		$this->form_validation->set_rules('registro_usuario', 'registro_usuario', 'trim|required');
		$this->form_validation->set_rules('registro_nombre', 'registro_nombre', 'trim|required');
		$this->form_validation->set_rules('registro_apellidos', 'registro_apellidos', 'trim|required');
		$this->form_validation->set_rules('registro_email', 'registro_email', 'trim|required|valid_email');
		$this->form_validation->set_rules('registro_reemail', 'registro_reemail', 'trim|required|valid_email');
		$this->form_validation->set_rules('registro_password', 'registro_password', 'trim|required|min_length[6]|max_length[16]');

	    $datos["usuario"] = $this->input->post('registro_usuario');
     	$datos["nombre"] = ucfirst($this->input->post('registro_nombre'));
     	$datos["apellidos"] = ucfirst($this->input->post('registro_apellidos'));
     	$datos["correo"] = strtolower($this->input->post('registro_email'));
     	$datos["recorreo"] = strtolower($this->input->post('registro_reemail'));
     	$datos["password"] = $this->input->post('registro_password');
		$submit = $this->input->post('submit');
		$datos["IP"] = $this->input->ip_address();
		
		$this->recordar();

		if ($this->session->userdata('Dokdas_User_ID')){
			//TIene una sesión iniciada 
		}else{
			//No tiene sesión iniciada
			if ($this->form_validation->run() == FALSE){
				if($submit==NULL){
					//No se habia mandado antes
					echo "no hay error , no hay error";
				}else{
					//si se habia mandado antes
					//error 1
					echo "llenar todos los datos";
				}
			}else{
				//Se mando el form correctamente
				if($datos["correo"]!=$datos["recorreo"]){
					//Correos son diferentes
					echo 'Correos diferentes';
				}else{
					//correos son iguales
					if($this->minicio->get_id_by_correo($datos["correo"])!=FALSE){
						//Correo utilizado
						echo 'Este correo ya esta en uso';
					}else{
						//correo nuevo
						if($this->minicio->get_true_by_usuario($datos["usuario"])==TRUE){
							//usuario ya registrado
							echo 'Este usuario ya esta registrado';
						}else{
							//usuario no registrado
							if($this->dokdas->password_seguro($datos["password"],$texto)==FALSE){
								echo $texto;
							}else{
								if(!$this->input->valid_ip($datos["IP"])){
									echo "IP no valida";
								}else{
									$datos["password"] = sha1($datos["password"]);
									//Clave unica que se usara en el mensaje de bienvenida
									
									
									$this->minicio->registrando($datos);
									$id = $this->minicio->get_id_by_correo_pass($datos);
									$id = $id["0"]["ID"];
									$email["Clave"] = rand(10,100).'-'.sha1($id).'-'.rand(10,100);
									$this->minicio->registrando_config($id,$email["Clave"]);
									
									$datasession = array(
										'Dokdas_User_ID' => $id,
							        	'Dokdas_User_Correo'  => $datos["correo"],
							        	'Dokdas_User_Ip' => $datos["IP"],
							         	'Dokdas_login_ok' => TRUE
							        );
							        $cookie_id = array(
									    'name'   => 'Dokdas_User_ID',
									    'value'  =>  $id,
									    'expire' => '180000',
									    //'domain' => 'www.dokdas.com',
									    'domain' => 'localhost',
									    'path'   => '/'
									);
									$cookie_correo = array(
									    'name'   => 'Dokdas_User_Correo',
									    'value'  =>  $datos["correo"],
									    'expire' => '180000',
									    //'domain' => 'www.dokdas.com',
									    'domain' => 'localhost',
									    'path'   => '/'
									    
									);
				
									$this->input->set_cookie($cookie_id); 
									$this->input->set_cookie($cookie_correo); 
									$this->session->set_userdata($datasession);
									
									$this->load->library('email');
									$email_setting  = array('mailtype'=>'html');
									$this->email->initialize($email_setting);
									$this->email->from('registro-noreply@dokdas.com', 'Dokdas');
									$this->email->to($datos["correo"]);
									
									$this->email->subject('Bienvenido a Dokdas');
									$email["Nombre"] = $this->input->post("nombre");
									
									
									$mensaje = $this->load->view('email/mensaje',$email,TRUE);
									$this->email->message($mensaje);
									$this->email->send();
									
									$this->email->print_debugger();
									
									redirect("inicio/gracias");	
												
									
								}
							}
						}
						
					}
				}
				
			}
			
		}
	}
	public function iniciando()
	{
		# code...
	}
	public function cerrando()
	{
		# code...
	}

	private function foto($servidor,$foto,$id){
		if($foto!=NULL){
			switch ($servidor) {
				case '0':
					$servidor = base_url();
					break;
				default:
					return $servidor = base_url();
					break;
			}
			return $servidor."img/".$id."/".$foto;
		}else{
			return base_url()."img/perfil.jpg";
		}
	}
	public function verificar_usuario(){
		if($this->input->is_ajax_request()){
			$data_verficicar["usuario"] = $this->input->get('usuario');
			
			if($data_verficicar["usuario"]!=NULL){
				if($this->minicio->existe_usuario($data_verficicar["usuario"])==TRUE){
					echo '<b class="text-danger">Este usuario ya esta registrado</b>';
				}else{
					echo '<b class="text-success">Usuario Verificado</b>';
				}
			}else{
				echo "<b class='text-danger'>Campo requerido</b>";
			}
		}else{
			echo "Nanais";
		}
	}

	public function verificar_correo(){


		$email = strtolower($this->input->get('correo', TRUE));
		if($email!=NULL){
			if ($this->dokdas->check_email_address($email)==TRUE){
			   if($this->minicio->existe_correo($email)==TRUE){
			   		echo "<b class='text-danger'>Este correo ya esta en uso.</b>";
			   }else{
			   		echo "<b class='text-success'>Correo válido.</b>";	
			   }
			} else{
			   echo "<b class='text-danger'>Correo invalido.</b>";
			}
		}else{
			echo "<b><p class='text-danger'>Campo requerido</p></b>";
		}
	}
	public function verificar_password(){


		$password = $this->input->get("password",TRUE);
		if($password!=NULL){
			if($this->dokdas->password_seguro($password,$texto)==TRUE){
				echo "<b class='text-success'>Contraseña confiable.</b>";
			}else{
				echo "<b><p class='text-danger'>$texto</p></b>";
			}
		}else{
			echo "<b><p class='text-danger'>Campo requerido</p></b>";
		}

	}
}

/* End of file inicio.php */
/* Location: ./application/controllers/inicio.php */