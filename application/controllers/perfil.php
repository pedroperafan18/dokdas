<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{
		$session["ID"] = 4;
		$dos = $this->uri->segment(2);
		$tres = $this->uri->segment(3);

		if(($dos!=NULL&&is_numeric($dos))||($tres!=NULL&&is_numeric($tres))){
			$numero =  (is_numeric($dos)) ? $dos : $tres;
			echo "Ver perfil de $numero";
			if($numero==$session["ID"]){
				redirect(base_url()."perfil");
			}
		}else{
			echo "ver mi perfil";
		}
	}

	public function seguir(){
		# code...
		
	}
	public function setranking(){
		# code...
	}
	public function getranking(){
		# code...
	}


}

/* End of file perfil.php */
/* Location: ./application/controllers/perfil.php */