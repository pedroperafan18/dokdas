<?php if (!defined('BASEPATH')) exit('No permitir el acceso directo al script'); 

class Dokdas{

    public function mes($mess)
	{
		switch($mess){
	      case 1:$mess = 'Enero';break;
	      case 2:$mess = 'Febrero';break;
	      case 3:$mess = 'Marzo';break;
	      case 4:$mess = 'Abril';break;
	      case 5:$mess = 'Mayo';break;
	      case 6:$mess = 'Junio';break;
	      case 7:$mess = 'Julio';break;
	      case 8:$mess = 'Agosto';break;
	      case 9:$mess = 'Septiembre';break;
	      case 10:$mess = 'Octubre';break;
	      case 11:$mess = 'Noviembre';break;
	      case 12:$mess = 'Diciembre';break;
	   }
		return $mess;
	}

	public function fecha($fecha){
	   $f0 = strtotime(date("Y-m-d H:i:s"));
	   $f1 = strtotime($fecha);
	   $seg = ($f0 - $f1);
	   if($seg > 60){
	   $min = $seg / 60; 
	   if($min > 60){
	   $hor = $min / 60; 
	   if($hor > 24){
	   $dia = $hor / 24;
	   if($dia > 7 ){
	   $dias = substr($fecha, 8, -8);
	   $anos = substr($fecha, 0, 4);
	   $mess = substr($fecha, 5, 6);
	   $hora = substr($fecha, 11, -3);
	   switch($mess){
	      case 1:$mess = 'Enero';break;
	      case 2:$mess = 'Febrero';break;
	      case 3:$mess = 'Marzo';break;
	      case 4:$mess = 'Abril';break;
	      case 5:$mess = 'Mayo';break;
	      case 6:$mess = 'Junio';break;
	      case 7:$mess = 'Julio';break;
	      case 8:$mess = 'Agosto';break;
	      case 9:$mess = 'Septiembre';break;
	      case 10:$mess = 'Octubre';break;
	      case 11:$mess = 'Noviembre';break;
	      case 12:$mess = 'Diciembre';break;
	   }
	  $resultado =  $dias.' de '.$mess.' del '.$anos.' a la(s) '.$hora;
	   }else{
	   $resultado = round($dia);
	   if($resultado==1){$resultado = 'Hace aproximadamente un d&iacute;a';}else{$resultado = 'Hace aproximadamente '.$resultado.' d&iacute;as';}
	   }
	   }else{
	   $resultado = round($hor);
	   if($resultado==1){$resultado = 'Hace aproximadamente una hora';}else{$resultado = 'Hace aproximadamente '.$resultado.' horas';}
	   }
	   }else{
	    $resultado = round($min);
		if($resultado==1){$resultado = 'Hace aproximadamente un minuto';}else{$resultado = 'Hace aproximadamente '.$resultado.' minutos';}
	   }
	   }else{
	   $resultado = 'Hace '.$seg.' segundos';
	   }
	
	   if(isset($resultado))
		return $resultado;
	}
	public function check_email_address($email){
		// Primero, checamos que solo haya un símbolo @, y que los largos sean correctos
	  if (!@ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) 
		{
			// correo inválido por número incorrecto de caracteres en una parte, o número incorrecto de símbolos @
	    return false;
	  }
	  // se divide en partes para hacerlo más sencillo
	  $email_array = explode("@", $email);
	  $local_array = explode(".", $email_array[0]);
	  for ($i = 0; $i < sizeof($local_array); $i++) 
		{
	    if (!@ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) 
			{
	      return false;
	    }
	  } 
	  // se revisa si el dominio es una IP. Si no, debe ser un nombre de dominio válido
		if (!@ereg("^\[?[0-9\.]+\]?$", $email_array[1])) 
		{ 
	     $domain_array = explode(".", $email_array[1]);
	     if (sizeof($domain_array) < 2) 
			 {
	        return false; // No son suficientes partes o secciones para se un dominio
	     }
	     for ($i = 0; $i < sizeof($domain_array); $i++) 
			 {
	        if (!@ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
					{
	           return false;
	        }
	     }
	  }
	  return true;
	}
	public function password_seguro($clave,&$error_clave){
	   if(strlen($clave) < 6){
	      $error_clave = "La clave debe tener al menos 6 caracteres";
	      return false;
	   }
	   if(strlen($clave) > 16){
	      $error_clave = "La clave no puede tener más de 16 caracteres";
	      return false;
	   }
	   if (!preg_match('`[a-z]`',$clave)){
	      $error_clave = "La clave debe tener al menos una letra minúscula";
	      return false;
	   }
	   if (!preg_match('`[A-Z]`',$clave)){
	      $error_clave = "La clave debe tener al menos una letra mayúscula";
	      return false;
	   }
	   if (!preg_match('`[0-9]`',$clave)){
	      $error_clave = "La clave debe tener al menos un caracter numérico";
	      return false;
	   }
	   $error_clave = "";
	   return true;
	} 
}