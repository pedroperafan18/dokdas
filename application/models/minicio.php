<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minicio extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}
	public function get_id_by_correo_pass($datos)
	{
		$this->db->select('ID');
		$this->db->where('Correo',$datos["correo"]);
		$this->db->where('Password',$datos["password"]);
		$query = $this->db->get('usuarios');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}
	public function get_true_by_usuario($usuario)
	{	
		$this->db->where('Usuario',$usuario);	
		$query = $this->db->get('usuarios');
		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function get_id_by_correo($correo){
		$this->db->select('ID');
		$this->db->where("Correo",$correo);
		$query = $this->db->get('usuarios');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	public function get_true_by_id_correo($id,$correo)
	{
	    $this->db->where('ID', $id);
	    $this->db->where('Correo', $correo);
	    $query = $this->db->get('usuarios');
        if ($query->num_rows() > 0){
          return TRUE;
        } else {
          return FALSE;
        }
	}
	public function registrando($datos)
	{
		$fecha = date("Y-m-d H:i:s"); 
		$data = array(
           'Nombre' => $datos["nombre"] ,
           'Apellidos' => $datos["apellidos"],
           'Correo' => $datos["correo"],
           'Password' => $datos["password"],
           'Usuario' => $datos["usuario"],
           'Creacion' => $fecha,
           'IP' => $datos["IP"],
           'Habilitado' => 2
        );
		
		$this->db->insert('usuarios', $data);
	}
	public function registrando_config($id,$clave)
	{
		$data = array(
			'ID_usuario' => $id,
			'Clave' => $clave
		);
		$this->db->insert('configuracion',$data);
	}
}

/* End of file minicio.php */
/* Location: ./application/models/minicio.php */